var unit_fakultas, m_formulir, type_form, value_sama_data;
        $(document).ready(function () {
            $("#select").on("change", function () {
                unit_fakultas       = $(this).find(":selected").text();
            });
            $("#pilihTypeForm").on("change", function () {
                type_form       = $(this).find(":selected").text();
            });
            $(".sama_data").on("click", function () {
                value_sama_data       = $(this).val();
            });
            $("#m_formulir").on("change", function () {
                m_formulir          = $('#m_formulir').find(":selected").val();
                $("#simpan").on("click", function () {
                    var periode         = $("#periode").val();
                    var start_date      = $("#start_date").val();
                    var end_date        = $("#end_date").val();
                    var nama_formulir   = $("#nama_formulir").val();
                    if (unit_fakultas == "Unit"){
                        var value        = $("#val_unit").val();
                    }else{
                        var value        = $("#val_fakultas").val();
                    }
                    $.ajax({
                        url: "{{ url('master-formulir') }}",
                        type: "POST",
                        data: {
                            "_token"                : "{{ csrf_token() }}",
                            "master_formulir"       : m_formulir,
                            "type_form"             : type_form,
                            "unit_fakultas"         : unit_fakultas,
                            "periode"               : periode,
                            "start_date"            : start_date,
                            "end_date"              : end_date,
                            "nama_formulir"         : nama_formulir,
                            "value_unit_fakultas"   : value,
                            "pilih_data"            : value_sama_data,
                        },
                        success:function(data){
                            if(data.success){
                                alert(data.success, window.location.href= "{{ URL('master-formulir') }}" );
                            }else{
                                alert('Create Data error, Name Formulir Already Exist');
                            }
                        }
                    });
                });
            });

            $("#select").on("change", function () {
                var type = $("#select").val();
                if (type == "unit") {
                    $("#unit").show();
                    $("#fakultas").hide();
                } else {
                    $("#unit").hide();
                    $("#fakultas").show();
                }
            });
        });
