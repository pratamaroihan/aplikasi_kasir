$(document).ready(function () {
    $("#addMore").click(function () {
        var fieldHTML = '<div class="card-body fieldGroup" style="margin-top:-20px;">' + $(".fieldGroupCopy").html() + '</div>';
        $('body').find('.fieldGroup:last').after(fieldHTML);
        var fieldHTML = '<div class="p-2 border mr-1 fieldGroup1">' + $(".fieldGroupCopy1").html() +'</div>';
        $('body').find('.fieldGroup1:last').after(fieldHTML);
    });


    $("body").on("click", "#remove", function () {
        let currow = $(this).closest('.fieldGroup');
        $(this).parents(".fieldGroup").remove();
    });

    $('#periode').on('change', function (e) {
        var periode_id = e.target.value;

        $.get('/getPeriode?id=' + periode_id, function (data) {
            $('#start_date').val(data[0].start_periode);
            $('#end_date').val(data[0].end_periode);
        });
    });


    $('#pilihTypeForm').on('change', function (e) {
        $("#select").each(function() { this.selectedIndex = 0 });
        var type = $("#pilihTypeForm").val();
            if (type == "new") {
                $("#importForm").hide();
                $("#newForm").show();
                $("#importdata").hide();
                // $("#formulirFor").show();
            } else  {
                $("#importdata").show();
                $("#importForm").show();
                $("#newForm").hide();
                // $("#formulirFor").show();
            }
    });


    // $(".selectedFor").on("change", function () {
    //     var type = $("#pilihTypeForm").val();
    //     if (type == "new") {
    //             $("#importForm").hide();
    //             $("#newForm").show();
    //             $("#importdata").hide();
    //     } else {
    //             $("#importdata").show();
    //             $("#importForm").show();
    //             $("#newForm").hide();
    //     }
    // });

});


