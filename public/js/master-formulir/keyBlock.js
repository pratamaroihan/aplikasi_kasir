document.querySelector(".formName").addEventListener("keypress", function (evt) {
    if (evt.which != 8 && evt.which != 0 && evt.which != 32 && evt.which < 48 || evt.which > 57 && evt.which < 65)
    {
        evt.preventDefault();
    }
});
