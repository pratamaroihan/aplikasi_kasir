<?php

use App\Http\Controllers\HistoriController;
use App\Http\Controllers\MBarangController;
use App\Http\Controllers\TBarangController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', function () {
    return view('home');
});

// Route::get('/master-barang', function () {
//     return view('data-master.barang.index');
// });

Route::middleware(['web'])->prefix('master')->group(function () {
    Route::resource('/barang', MBarangController::class);
});


Route::middleware(['web'])->prefix('transaksi')->group(function () {
    Route::resource('/pembelian-barang', TBarangController::class);
});

Route::middleware(['web'])->prefix('histori')->group(function () {
    Route::resource('/', HistoriController::class);
});


//js get produk
Route::get('/getBarang', [MBarangController::class, 'getBarangId']);
