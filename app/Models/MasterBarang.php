<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MasterBarang extends Model
{
    // use HasFactory;

    protected $fillable = [
        'nama_barang',
        'harga_satuan',
        'created_at',
        'updated_at',
    ];

    public function master_barang()
    {
        return $this->hasMany(TransaksiPembelianBarang::class, 'master_barang_id', 'id');
    }

    // public function komposisi()
    // {
    //     return $this->hasToMany(MKomposisi::class, 'm_bahan_baku_id', 'id');
    // }

}
