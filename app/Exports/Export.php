<?php

namespace App\Exports;

use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\FromCollection;

class Export implements FromCollection
{
    public function collection()
    {
        return DB::table('pendaftaran_jurusan_teknik_informatika')->get();
    }
}
