<?php

namespace App\Exports;

use App\Models\pendaftaran_fakultas_teknik_ilmu_komputers;
use Maatwebsite\Excel\Concerns\FromCollection;

class pendaftaranfakultasteknikilmukomputersexport implements FromCollection
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return pendaftaran_fakultas_teknik_ilmu_komputers::all();
    }
}
