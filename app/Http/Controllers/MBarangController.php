<?php

namespace App\Http\Controllers;

use App\Models\MasterBarang;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\DB;

class MBarangController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        DB::beginTransaction();
        try {
            $master_barang = MasterBarang::get();
            DB::commit();
            return view('data-master.barang.index', compact('master_barang'));
        } catch (\Throwable $th) {
            //throw $th;
            DB::rollback();
            return redirect('/master/barang')->with(['error', 'Error pull data']);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $id = Crypt::decrypt($id);
        DB::beginTransaction();
        try {
            DB::commit();
            $master_barang = MasterBarang::find($id);
            return view('data-master.barang.show', compact('master_barang'));
        } catch (\Throwable $th) {
            //throw $th;
            DB::rollback();
            return redirect('/master/barang')->with(['error', 'Error pull data']);
        }

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function getBarangId(Request $request)
    {
        $id_barang          = $request->input('id');
        $getDataBarang      = MasterBarang::where('id', $id_barang)->get();

        return response()->json($getDataBarang);
    }
}
