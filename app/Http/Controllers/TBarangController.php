<?php

namespace App\Http\Controllers;

use App\Models\MasterBarang;
use App\Models\TransaksiPembelian;
use App\Models\TransaksiPembelianBarang;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\DB;

class TBarangController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        DB::beginTransaction();
        try {
            $transaksi_pembelian = TransaksiPembelianBarang::get();
            DB::commit();
            return view('transaksi.barang.index', compact('transaksi_pembelian'));
        } catch (\Throwable $th) {
            //throw $th;
            DB::rollback();
            return redirect('/transaksi/pembelian-barang')->with('error', 'Error pull data');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        DB::beginTransaction();
        try {
            $master_barang = MasterBarang::get();
            DB::commit();
            return view('transaksi.barang.create', compact('master_barang'));
        } catch (\Throwable $th) {
            //throw $th;
            DB::rollback();
            return redirect('/master/barang')->with('error', 'Error pull data');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'barang'      => ['required'],
            'harga_satuan'  => ['required'],
            'jumlah'     => ['required'],
            'total_harga'  => ['required'],
        ]);

        DB::beginTransaction();
        try {
            $harga_barang = TransaksiPembelian::create([
                'total_harga' => $request->total_harga,
            ]);

            $transaksi_barang = TransaksiPembelianBarang::create([
                'master_barang_id' => $request->barang,
                'transaksi_pembelian_id' => $harga_barang->id,
                'jumlah' => $request->jumlah,
                'harga_satuan' => $request->harga_satuan,
            ]);

            DB::commit();
            return redirect('transaksi/pembelian-barang')->with('success', 'Success create data');
        } catch (\Exception $e) {
            DB::rollback();
            return redirect('transaksi/pembelian-barang')->with('error', 'Error create data');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
