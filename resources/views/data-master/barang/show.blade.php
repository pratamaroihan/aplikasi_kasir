@extends('layouts.layouts-admin.layouts-master')
@section('title', 'Data Barang')
@section('content')
<div class="nk-content ">
    <div class="container-fluid">
        <div class="nk-content-inner">
            <div class="nk-content-body">
                <div class="nk-block-head nk-block-head-sm">
                    <nav>
                        <ul class="breadcrumb breadcrumb-pipe">
                            <li class="breadcrumb-item"><a href="#">{{ __('Dashboard') }}</a></li>
                            <li class="breadcrumb-item"><a href="{{URL('master/barang')}}">{{ __('Master Data Barang') }}</a></li></li>
                            <li class="breadcrumb-item active">{{ __('Detail Data Barang') }}</li>
                        </ul>
                    </nav>
                    <div class="nk-block-between">
                        <div class="nk-block-head-content">
                            <h3 class="nk-block-title page-title">{{ __('Detail Data Barang') }}</h3>
                        </div>
                    </div>
                </div>
                <div class="card"> <!-- .for use card "card" -->
                    <div class="card-inner"> <!-- .for use card "card-inner" -->
                        <div class="nk-block">
                            <div class="row">
                                <div class="col-sm-6 mt-4">
                                    <div class="form-group">
                                        <label class="form-label" for="default-01">Nama Barang</label>
                                        <div class="form-control-wrap">
                                            <input type="text" class="form-control" value="{{$master_barang->nama_barang}}" readonly>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6 mt-4">
                                    <div class="form-group">
                                        <label class="form-label" for="default-01">Harga Satuan</label>
                                        <div class="form-control-wrap">
                                            <input type="text" class="form-control" value="Rp. {{$master_barang->harga_satuan}}" readonly>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6 mt-4">
                                    <div class="form-group">
                                        <label class="form-label" for="default-01">Dibuat Pada</label>
                                        <div class="form-control-wrap">
                                            <input type="text" class="form-control" value="{{date('d F Y - H:i:s', strtotime($master_barang->created_at))}}" readonly>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6 mt-4">
                                    <div class="form-group">
                                        <label class="form-label" for="default-01">Diperbaharui Pada</label>
                                        <div class="form-control-wrap">
                                            <input type="text" class="form-control" value="{{isset($master_barang->updated_by) ? date('d F Y - H:i:s', strtotime($master_barang->updated_by)) : '-'}}" readonly>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
