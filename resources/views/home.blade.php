@extends('layouts.layouts-admin.layouts-master')
@section('content')
{{-- <div class="nk-content ">
    <div class="container-fluid">
        <div class="nk-content-inner">
            <div class="nk-content-body">
                <div class="nk-block-head nk-block-head-sm">
                    <div class="nk-block-between">
                        <div class="nk-block-head-content">
                            <h3 class="nk-block-title page-title">{{ __('Dashboard') }}</h3>
                        </div>
                        <div class="nk-block-head-content">
                            <div class="toggle-wrap nk-block-tools-toggle">
                                <a class="btn btn-icon btn-trigger toggle-expand mr-n1" data-target="pageMenu">
                                    <em class="icon ni ni-more-v"></em>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="nk-block">
                    <div class="row g-gs">
                        @can("dashboard admin")
                            <div class="col-12 col-sm-6 col-md-6 col-lg-3 col-xl-3">
                                <div class="card">
                                    <div class="nk-ecwg nk-ecwg6">
                                        <div class="card-inner">
                                            <div class="card-title-group">
                                                <div class="card-title">
                                                    <h6 class="title text-primary" style="letter-spacing: 1px;">Formulir</h6>
                                                </div>
                                            </div>
                                            <div class="data">
                                                <div class="data-group">
                                                    <div class="amount">{{$total_formulir}}</div>
                                                </div>
                                                <div class="info">Total Formulir</span></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endcan

                        
                        <div class="col-12 col-sm-6 col-md-6 col-lg-3 col-xl-3">
                            <div class="card">
                                <div class="nk-ecwg nk-ecwg6">
                                    <div class="card-inner">
                                        <div class="card-title-group">
                                            <div class="card-title">
                                                <h6 class="title text-pink" style="letter-spacing: 1px;">Formulir Fakultas</h6>
                                            </div>
                                        </div>
                                        <div class="data">
                                            <div class="data-group">
                                                <div class="amount">{{$formulir_fakultas}}</div>
                                            </div>
                                            <div class="info">Jumlah Formulir Fakultas</span></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-sm-6 col-md-6 col-lg-3 col-xl-3">
                            <div class="card">
                                <div class="nk-ecwg nk-ecwg6">
                                    <div class="card-inner">
                                        <div class="card-title-group">
                                            <div class="card-title">
                                                <h6 class="title text-info" style="letter-spacing: 1px;">Formulir Unit</h6>
                                            </div>
                                        </div>
                                        <div class="data">
                                            <div class="data-group">
                                                <div class="amount">{{$formulir_unit}}</div>
                                            </div>
                                            <div class="info">Jumlah Formulir Unit</span></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>


                        @can("dashboard admin")
                            <div class="col-12 col-sm-6 col-md-6 col-lg-3 col-xl-3">
                                <div class="card">
                                    <div class="nk-ecwg nk-ecwg6">
                                        <div class="card-inner">
                                            <div class="card-title-group">
                                                <div class="card-title">
                                                    <h6 class="title text-danger" style="letter-spacing: 1px;">Pengguna</h6>
                                                </div>
                                            </div>
                                            <div class="data">
                                                <div class="data-group">
                                                    <div class="amount">{{$jmh_pengguna}}</div>
                                                </div>
                                                <div class="info">Total Pengguna</span></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endcan
                    </div>


                    <div class="row g-gs">
                        <div class="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6">
                            <div class="card h-100">
                                <div class="card-inner">
                                    <div class="card-title-group mb-2">
                                        <div class="card-title">
                                            <h6 class="title">Unit - Fakultas</h6>
                                        </div>
                                    </div>
                                    <ul class="nk-store-statistics mt-4">
                                        <li class="item">
                                            <div class="info">
                                                <div class="title">Jumlah Fakultas</div>
                                                <h4 class="mt-1">{{$jmh_fakultas}}</h4>
                                            </div>
                                            <em class="icon bg-primary-dim ni ni-archived"></em>
                                        </li>
                                        <li class="item">
                                            <div class="info">
                                                <div class="title">Jumlah Unit</div>
                                                <h4 class="mt-1">{{$jmh_unit}}</h4>
                                            </div>
                                            <em class="icon bg-info-dim ni ni-panel"></em>
                                        </li>
                                        <li class="item">
                                            <div class="info">
                                                <div class="title">Pengguna Fakultas</div>
                                                <h4 class="mt-1">{{$jmh_user_fakultas}}</h4>
                                            </div>
                                            <em class="icon bg-pink-dim ni ni-user-c"></em>
                                        </li>
                                        <li class="item">
                                            <div class="info">
                                                <div class="title">Pengguna Unit</div>
                                                <h4 class="mt-1">{{$jmh_user_unit}}</h4>
                                            </div>
                                            <em class="icon bg-purple-dim ni ni-user-c"></em>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>

                        
                        @can("dashboard admin")
                            <div class="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6">
                                <div class="card h-100">
                                    <div class="card-inner border-bottom">
                                        <div class="card-title-group">
                                            <div class="card-title">
                                                <h6 class="title">Formulir Baru</h6>
                                            </div>
                                        </div>
                                    </div>
                                    @foreach ($formulir->slice(0, 3) as $new_formulir)
                                    <ul class="nk-support">
                                        <li class="nk-support-item">
                                            <div class="user-avatar {{isset($new_formulir->master_fakultas_id ) ? 'bg-pink' : 'bg-info' }}">
                                                <span>{{isset($new_formulir->master_fakultas_id ) ? 'F' : 'U' }}</span>
                                            </div>
                                            <div class="nk-support-content">
                                                <div class="title" style="letter-spacing: 1px">
                                                    <span>
                                                        @if (strlen($new_formulir->name) > 25 )
                                                            {{ substr($new_formulir->name,0, 25)."..." }}
                                                        @else
                                                            {{ $new_formulir->name }}
                                                        @endif
                                                    </span>
                                                    <span class="badge badge-dot badge-dot-xs badge-success ml-1">Active</span>
                                                </div>
                                                <p>Untuk {{isset($new_formulir->master_fakultas_id ) ? 'fakultas' : 'unit' }}  @if (strlen($new_formulir->name) > 25 ) {!! substr($new_formulir->name,0, 25)."..." !!} @else {{ $new_formulir->name }} @endif</p>
                                                <span class="time">{{$new_formulir->created_at->diffForHumans()}}</span>
                                            </div>
                                        </li>
                                    </ul>
                                    @endforeach
                                </div>
                            </div>
                        @endcan
                    </div>


                    <div class="row g-gs">
                        @can("dashboard admin")
                            <div class="col-12 col-md-12 col-lg-12 col-xl-12">
                                <div class="card card-full">
                                    <div class="card-inner">
                                        <div class="card-title-group">
                                            <div class="card-title">
                                                <h6 class="title">Pengguna Baru</h6>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="nk-tb-list mt-4">
                                        <div class="nk-tb-item nk-tb-head">
                                            <div class="nk-tb-col"><span>ID</span></div>
                                            <div class="nk-tb-col "><span>Nama Lengkap</span></div>
                                            <div class="nk-tb-col tb-col-md"><span>Tanggal Reg</span></div>
                                            <div class="nk-tb-col tb-col-sm"><span>Tipe</span></div>
                                            <div class="nk-tb-col tb-col-sm"><span>Unit - Fakultas</span></div>
                                        </div>
                                        @foreach ($pengguna->slice(0 , 5) as $new_pengguna)
                                            <div class="nk-tb-item">
                                                <div class="nk-tb-col">
                                                    <span class="tb-lead"><a href="#">#{{$new_pengguna->id}}</a></span>
                                                </div>
                                                <div class="nk-tb-col">
                                                    <div class="user-card">
                                                        <div class="user-avatar sm bg-purple-dim">
                                                            <span>{{ strtoupper(substr($new_pengguna->name,0, 2)) }}</span>
                                                        </div>
                                                        <div class="user-name">
                                                            <span class="tb-lead">{{$new_pengguna->name}}</span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="nk-tb-col tb-col-md">
                                                    <span class="tb-sub">{{date('d F Y', strtotime($new_pengguna->created_at))}}</span>
                                                </div>
                                                <div class="nk-tb-col tb-col-sm">
                                                    <span class="tb-sub tb-amount">{{isset($new_pengguna->user_detail->master_fakultas_id ) ? (isset($new_pengguna->user_detail->master_fakultas->id) ? 'Fakultas' : 'Admin') : (isset($new_pengguna->user_detail->master_units->name) ? 'Unit' : 'Admin')}}</span>
                                                </div>
                                                <div class="nk-tb-col tb-col-sm">
                                                    <span class="badge badge-dot badge-dot-xs badge-primary">{{isset($new_pengguna->user_detail->master_fakultas_id ) ? (isset($new_pengguna->user_detail->master_fakultas->name) ? $new_pengguna->user_detail->master_fakultas->name : '-') : (isset($new_pengguna->user_detail->master_units->name) ? $new_pengguna->user_detail->master_units->name : '-')}}</span>
                                                </div>
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        @endcan
                    </div>
                </div>
            </div>
        </div>
    </div>
</div> --}}
@endsection