test
@extends('layouts.layouts-admin.layouts-master')
@section('title', 'Transaksi Barang')
@section('content')
<div class="nk-content ">
    <div class="container-fluid">
        <div class="nk-content-inner">
            <div class="nk-content-body">
                <div class="nk-block-head nk-block-head-sm">
                    <nav>
                        <ul class="breadcrumb breadcrumb-pipe">
                            <li class="breadcrumb-item"><a href="#">{{ __('Dashboard') }}</a></li>
                            <li class="breadcrumb-item active">{{ __('Transaksi Data Barang') }}</li>
                        </ul>
                    </nav>
                </div>
                <div class=""> <!-- .for use card "card" -->
                    <div class=""> <!-- .for use card "card-inner" -->
                        <div class="nk-block">
                            <table class="datatable-init nowrap nk-tb-list is-separate" data-auto-responsive="false">
                                <thead>
                                <tr class="nk-tb-item nk-tb-head">
                                    <th class="nk-tb-col nk-tb-col-check">{{ __('No') }}</th>
                                    <th class="nk-tb-col">{{ __('ID Transaksi') }}</th>
                                    <th class="nk-tb-col">{{ __('Nama Barang') }}</th>
                                    <th class="nk-tb-col">{{ __('Harga Satuan') }}</th>
                                    <th class="nk-tb-col">{{ __('Jumlah') }}</th>
                                    <th class="nk-tb-col">{{ __('Total Harga') }}</th>
                                    <th class="nk-tb-col">{{ __('Pembelian Pada') }}</th>
                                    {{-- <th class="nk-tb-col nk-tb-col-tools">
                                        <ul class="nk-tb-actions gx-1 my-n1">
                                            <li class="mr-n1">
                                                <div>
                                                    <a class="btn btn-icon"><em class="icon ni ni-more-h"></em></a>
                                                </div>
                                            </li>
                                        </ul>
                                    </th> --}}
                                </tr><!-- .nk-tb-item -->
                                </thead>
                                <tbody>
                                    @foreach ( $transaksi_pembelian as $data)
                                        <tr class="nk-tb-item" >
                                            <td class="nk-tb-col nk-tb-col-check">{{$loop->iteration}}</td>
                                            <td class="nk-tb-col">{{$data->id}}</td>
                                            <td class="nk-tb-col">{{$data->master_barang->nama_barang}}</td>
                                            <td class="nk-tb-col">{{$data->harga_satuan}}</td>
                                            <td class="nk-tb-col">{{$data->jumlah}}</td>
                                            <td class="nk-tb-col">{{$data->transaksi_pembelian->total_harga}}</td>
                                            <td class="nk-tb-col">{{date('d F Y - H:i:s', strtotime($data->created_at))}}</td>
                                            {{-- <td class="nk-tb-col nk-tb-col-check">
                                                <div class="nk-tb-actions gx-1">
                                                    <div class="tb-odr-btns d-none d-md-inline">
                                                        <button type="button" class="btn btn-icon btn-round  btn-success" disabled>
                                                            <em class="icon ni ni-eye"></em>
                                                        </button>
                                                        <button type="button" class="btn btn-dim btn-icon btn-round  btn-outline-primary" disabled>
                                                            <em class="icon ni ni-edit"></em>
                                                        </button>
                                                        <button type="button" class="btn btn-dim btn-icon btn-round btn-outline-danger" disabled>
                                                            <em class="icon ni ni-trash-empty"></em>
                                                        </button>
                                                    </div>
                                                </div>
                                            </td> --}}
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
