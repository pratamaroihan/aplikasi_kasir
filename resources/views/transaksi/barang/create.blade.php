@extends('layouts.layouts-admin.layouts-master')
@section('title', 'Beli Barang')
@section('content')
<div class="nk-content ">
    <div class="container-fluid">
        <div class="nk-content-inner">
            <div class="nk-content-body">
                <div class="nk-block-head nk-block-head-sm">
                    <nav>
                        <ul class="breadcrumb breadcrumb-pipe">
                            <li class="breadcrumb-item"><a href="#">{{ __('Dashboard') }}</a></li>
                            <li class="breadcrumb-item"><a href="{{URL('master/barang')}}">{{ __('Data Transaksi') }}</a></li></li>
                            <li class="breadcrumb-item active">{{ __('Beli Barang') }}</li>
                        </ul>
                    </nav>
                    <div class="nk-block-between">
                        <div class="nk-block-head-content">
                            <h3 class="nk-block-title page-title">{{ __('Beli Barang') }}</h3>
                        </div>
                    </div>
                </div>
                <form action="{{URL('transaksi/pembelian-barang')}}" method="post">
                    @csrf
                    @method("post")
                    <div class="card col-md-6"> <!-- .for use card "card" -->
                        <div class="card-inner"> <!-- .for use card "card-inner" -->
                            <div class="nk-block">
                                <div class="row">
                                    <div class="col-md-12 mt-4">
                                        <div class="form-group">
                                            <label class="form-label">Pilih Data Barang</label>
                                            <div class="form-control-wrap">
                                                <select id="get_barang" name="barang" class="form-select select2" tabindex="-1" aria-hidden="true">
                                                    <option selected disabled>Silahkan pilih barang</option>
                                                    @foreach ($master_barang as $data)
                                                        <option value="{{$data->id}}">{{$data->nama_barang}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-8 mt-4">
                                        <div class="form-group">
                                            <label class="form-label">Harga Satuan</label>
                                            <div class="form-control-wrap">
                                                <input type="text" id="get_harga_satuan" name="harga_satuan" class="form-control" value="{{old('harga_satuan')}}" readonly required>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4 mt-4">
                                        <div class="form-group">
                                            <label class="form-label">Jumlah</label>
                                            <div class="form-control-wrap">
                                                <input type="number" id="jumlah" name="jumlah" min="0" class="form-control" value="{{old('jumlah')}}" disabled required>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12 mt-4">
                                        <div class="form-group">
                                            <label class="form-label">Total Harga</label>
                                            <div class="form-control-wrap">
                                                <input type="text" id="total_harga" name="total_harga" class="form-control" readonly required>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-footer bg-white text-right">
                            <button id="button_simpan" class="btn btn-success" disabled> Beli Barang </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
<script>
    $(document).ready(function () {
        $('#get_barang').change(function (e) {
            $('#jumlah').prop("disabled", false);
            $('#button_simpan').prop("disabled", false);
            var id_barang = e.target.value;
            console.log(id_barang);

            $.get('/getBarang?id=' + id_barang, function (data) {
            console.log(data);
                $('#get_harga_satuan').val(data[0].harga_satuan);
                // $('#end_date').val(data[0].end_periode);
            });
        });

        $('#jumlah').keyup(function (e) {
            var get_harga_satuan = $('#get_harga_satuan').val();
            var get_jumlah = $('#jumlah').val();
            $("#total_harga").val(get_harga_satuan * get_jumlah);
        });

    });

</script>
