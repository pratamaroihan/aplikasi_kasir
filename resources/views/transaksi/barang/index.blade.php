test
@extends('layouts.layouts-admin.layouts-master')
@section('title', 'Transaksi Barang')
@section('content')
<div class="nk-content ">
    <div class="container-fluid">
        <div class="nk-content-inner">
            <div class="nk-content-body">
                <div class="nk-block-head nk-block-head-sm">
                    <nav>
                        <ul class="breadcrumb breadcrumb-pipe">
                            <li class="breadcrumb-item"><a href="#">{{ __('Dashboard') }}</a></li>
                            <li class="breadcrumb-item active">{{ __('Transaksi Data Barang') }}</li>
                        </ul>
                    </nav>
                    <div class="nk-block-between">
                        <div class="nk-block-head-content">
                            <h3 class="nk-block-title page-title">{{ __('Transaksi Barang') }}</h3>
                        </div>
                        @if (session('success'))
                            <div class="mt-2">
                                <div class="alert alert-success p-1">
                                    {{ session('success') }}
                                </div>
                            </div>
                        @endif
                        @if (session('error'))
                            <div class="mt-2">
                                <div class="alert alert-danger p-1">
                                    {{ session('error') }}
                                </div>
                            </div>
                        @endif
                        <div class="nk-block-head-content">
                            <div class="toggle-wrap nk-block-tools-toggle">
                                <a class="btn btn-icon btn-trigger toggle-expand mr-n1" data-target="pageMenu">
                                    <em class="icon ni ni-more-v"></em>
                                </a>
                                <div class="toggle-expand-content" data-content="pageMenu">
                                    <div class="nk-block-tools g-3">
                                        <div class="nk-block-tools-opt">
                                            <a href="{{URL('transaksi/pembelian-barang/create')}}" type="button" class="btn btn-primary" disabled>
                                                <em class="icon ni ni-plus-sm"></em>
                                                <span>{{ __('Beli Barang') }}</span>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class=""> <!-- .for use card "card" -->
                    <div class=""> <!-- .for use card "card-inner" -->
                        <div class="nk-block">
                            <table class="datatable-init nowrap nk-tb-list is-separate" data-auto-responsive="false">
                                <thead>
                                <tr class="nk-tb-item nk-tb-head">
                                    <th class="nk-tb-col nk-tb-col-check">{{ __('No') }}</th>
                                    <th class="nk-tb-col">{{ __('ID Transaksi') }}</th>
                                    <th class="nk-tb-col">{{ __('Waktu Transaksi') }}</th>
                                    <th class="nk-tb-col">{{ __('Total Harga') }}</th>
                                    <th class="nk-tb-col nk-tb-col-tools">
                                        <ul class="nk-tb-actions gx-1 my-n1">
                                            <li class="mr-n1">
                                                <div>
                                                    <a class="btn btn-icon"><em class="icon ni ni-more-h"></em></a>
                                                </div>
                                            </li>
                                        </ul>
                                    </th>
                                </tr><!-- .nk-tb-item -->
                                </thead>
                                <tbody>
                                    @foreach ( $transaksi_pembelian as $data)
                                        <tr class="nk-tb-item" >
                                            <td class="nk-tb-col nk-tb-col-check">{{$loop->iteration}}</td>
                                            <td class="nk-tb-col">{{$data->id}}</td>
                                            <td class="nk-tb-col">{{date('d F Y - H:i:s', strtotime($data->created_at))}}</td>
                                            <td class="nk-tb-col">{{$data->transaksi_pembelian->total_harga}}</td>
                                            <td class="nk-tb-col nk-tb-col-check">
                                                <div class="nk-tb-actions gx-1">
                                                    <div class="tb-odr-btns d-none d-md-inline">
                                                        <button type="button" class="btn btn-icon btn-round  btn-success" disabled>
                                                            <em class="icon ni ni-eye"></em>
                                                        </button>
                                                        <button type="button" class="btn btn-dim btn-icon btn-round  btn-outline-primary" disabled>
                                                            <em class="icon ni ni-edit"></em>
                                                        </button>
                                                        <button type="button" class="btn btn-dim btn-icon btn-round btn-outline-danger" disabled>
                                                            <em class="icon ni ni-trash-empty"></em>
                                                        </button>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
