<div id="sideBar" class="nk-sidebar nk-sidebar-fixed is-light" data-content="sidebarMenu">
    <div class="nk-sidebar-element nk-sidebar-head">
        <div class="nk-sidebar-brand">
            <a href="/demo2/index.html" class="logo-link nk-sidebar-logo">
                <img class="logo-light logo-img" src="{{URL('/app-assets/images/logo.png')}}" srcset="{{URL('images/logo2x.png 2x')}}" alt="logo">
                <img class="logo-dark logo-img" src="{{URL('/app-assets/images/logo-dark.png')}}" srcset="{{URL('images/logo-dark2x.png 2x')}}" alt="logo-dark">
                <img class="logo-small logo-img logo-img-small" src="{{URL('/app-assets/images/logo-small.png')}}" srcset="{{URL('images/logo-small2x.png 2x')}}" alt="logo-small"></a>
        </div>
        <div class="nk-menu-trigger mr-n2">
            <a class="nk-nav-toggle nk-quick-nav-icon d-xl-none" data-target="sidebarMenu"><em class="icon ni ni-arrow-left"></em></a>
            <a id="sideBarIcon" class="nk-nav-compact nk-quick-nav-icon d-none d-xl-inline-flex" data-target="sidebarMenu"><em class="icon ni ni-menu"></em></a>
        </div>
    </div>
    <div class="nk-sidebar-element">
        <div class="nk-sidebar-content">
            <div class="nk-sidebar-menu" data-simplebar>
                <ul class="nk-menu">
                    <li class="nk-menu-heading">
                        <h6 class="overline-title text-primary-alt">{{ __('Beranda') }}</h6>
                    </li>
                    <li class="nk-menu-item">
                        <a href="#" class="nk-menu-link">
                            <span class="nk-menu-icon">
                                <em class="icon ni ni-home-alt"></em>
                            </span>
                            <span class="nk-menu-text">{{ __('Dashboard') }}</span>
                        </a>
                    </li>

                    <li class="nk-menu-heading">
                        <h6 class="overline-title text-primary-alt">{{ __('Data Master') }}</h6>
                    </li>
                    <li class="nk-menu-item">
                        <a href="{{URL('master/barang')}}" class="nk-menu-link">
                            <span class="nk-menu-icon">
                                <em class="icon ni ni-cart"></em>
                            </span>
                            <span class="nk-menu-text">{{ __('Master Barang') }}</span>
                        </a>
                    </li>

                    <li class="nk-menu-heading">
                        <h6 class="overline-title text-primary-alt">{{ __('Data Transaksi') }}</h6>
                    </li>
                    <li class="nk-menu-item">
                        <a href="{{(URL('transaksi/pembelian-barang'))}}" class="nk-menu-link">
                            <span class="nk-menu-icon">
                                <em class="icon ni ni-wallet-alt"></em>
                            </span>
                            <span class="nk-menu-text">{{ __('Pembelian Barang') }}</span>
                        </a>
                    </li>
                    <li class="nk-menu-item">
                        <a href="{{(URL('histori'))}}" class="nk-menu-link">
                            <span class="nk-menu-icon">
                                <em class="icon ni ni-cart"></em>
                            </span>
                            <span class="nk-menu-text">{{ __('Histori Pembelian') }}</span>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
