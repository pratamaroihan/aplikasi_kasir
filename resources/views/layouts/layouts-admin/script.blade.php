<script src="{{ URL('app-assets/assets/js/bundle.js?ver=2.4.0') }}"></script>
<script src="{{ URL('app-assets/assets/js/scripts.js?ver=2.4.0') }}"></script>
<script src="{{ URL('app-assets/assets/js/charts/chart-ecommerce.js?ver=2.4.0') }}"></script>
@stack('js')
