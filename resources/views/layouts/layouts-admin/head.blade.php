<head>
    <base href="../">
    <meta charset="utf-8">
    <meta name="author">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Dashboards Base.">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <!-- Fav Icon  -->
    {{-- <link rel="shortcut icon" href="{{ URL('app-assets/images/favicon.png') }}"> --}}

    <!-- Page Title  -->
    <title>@yield('title')</title>
    <!-- StyleSheets  -->
    <link rel="stylesheet" href="{{ URL('app-assets/assets/css/dashlite.css') }}">
    <link id="skin-default" rel="stylesheet" href="{{ URL('app-assets/assets/css/theme.css?ver=2.4.0') }}">

    @stack('css')
</head>
