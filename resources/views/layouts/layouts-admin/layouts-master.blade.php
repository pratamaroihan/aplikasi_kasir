<!DOCTYPE html>
<html lang="en" class="js">

@include('layouts.layouts-admin.head')
    <body class="nk-body bg-lighter npc-default has-sidebar">
        <div class="nk-app-root">
            <div class="nk-main ">
                @include('layouts.layouts-admin.side')
                <div class="nk-wrap ">
                    @include('layouts.layouts-admin.header')
                    @yield('content')
                </div>
            </div>
        </div>
    @include('layouts.layouts-admin.footer')
    @include('layouts.layouts-admin.script')

    </body>
</html>
